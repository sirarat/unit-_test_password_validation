const { checkLenght, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLenght('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLenght('1234567')).toBe(false)
  })
  test('should 25 characters to be true', () => {
    expect(checkLenght('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLenght('11111111111111111111111111')).toBe(false)
  })
})
describe('Test Alphabet in password', () => {
  test('should has alphabet m in password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A in password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})
describe('Test Digit in password', () => {
  test('should has digit 0-9 in password', () => {
    expect(checkDigit('0')).toEqual(true)
  })
  test('should has not  digit 0-9 in password', () => {
    expect(checkDigit('ssss')).toEqual(false)
  })
})
describe('Test symbol in password', () => {
  test('should has symbol  ! in password to be true ', () => {
    expect(checkSymbol('!kkk')).toEqual(true)
  })
  test('should has symbol  _ in password to be true ', () => {
    expect(checkSymbol('_kkk')).toEqual(true)
  })
  test('should has not symbol in password', () => {
    expect(checkSymbol('bdx')).toEqual(false)
  })
})
describe('Test password', () => {
  test('should password sirarat_00329 to be true ', () => {
    expect(checkPassword('sirarat_00329')).toEqual(true)
  })
  test('should password fah@0 to be false ', () => {
    expect(checkPassword('fah@0')).toEqual(false)
  })
  test('should password fah_fah to be false ', () => {
    expect(checkPassword('fah_fah')).toEqual(false)
  })
  test('should password fah00329 to be false ', () => {
    expect(checkPassword('fah00329')).toEqual(false)
  })
  test('should password _00329 to be false ', () => {
    expect(checkPassword('_00329')).toEqual(false)
  })
  test('should password 00329 to be false ', () => {
    expect(checkPassword('00329')).toEqual(false)
  })
})
